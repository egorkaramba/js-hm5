//1)
// function привіт() {
//     console.log("Привіт!");
// }
// привіт();
// 2)return в JavaScript використовується для повернення значення з функції.
// function додавання(a, b) {
//     let сума = a + b;
//     return сума;

// }
// let результатДодавання = додавання(1, 2);
// console.log("Результат додавання:", результатДодавання);
// 3)використовуються для опису вхідних значень функції.
// Параметри - це змінні, оголошені в тілі функції, які визначаються при оголошенні функції і вказують, які значення повинні бути передані у функцію під час її виклику.
// Аргументи - це конкретні значення, які передаються у функцію при її виклику. Ці значення призначаються параметрам функції.
//  4)function викДію(функція, аргумент) {
//    функція(аргумент);
// }

// 1)
// function getNumber() {
//     let number;
//     do {
//         number = prompt("Введіть число:");
//         number = parseFloat(number);
//     } while (isNaN(number));
//     return number;
// }

// let number1 = getNumber();
// let number2 = getNumber();

// let operation;
// do {
//     operation = prompt("Введіть математичну операцію (+, -, *, /):");
//     if (!["+", "-", "*", "/"].includes(operation)) {
//         alert("Такої операції не існує");
//     }
// } while (!["+", "-", "*", "/"].includes(operation));

// function calculate(number1, number2, operation) {
//     switch (operation) {
//         case "+":
//             return number1 + number2;
//         case "-":
//             return number1 - number2;
//         case "*":
//             return number1 * number2;
//         case "/":
//             return number1 / number2;
//         default:
//             return NaN;
//     }
// }

// let result = calculate(number1, number2, operation);
// console.log(`result: ${result}`);

// 2)
function getNumber() {
    let number;
    do {
        number = prompt("Введіть число:");
        number = parseFloat(number);
    } while (isNaN(number));
    return number;
}
function getSurgery() {
    let operation;
    do {
        operation = prompt("Введіть математичну операцію (+, -, *, /):");
        if (!["+", "-", "*", "/"].includes(operation)) {
            alert("Такої операції не існує");
        }
    } while (!["+", "-", "*", "/"].includes(operation));
    return operation;
}
function perfomOperation(number1, number2, operation) {
    switch (operation) {
        case "+":
            return number1 + number2;
        case "-":
            return number1 - number2;
        case "*":
            return number1 * number2;
        case "/":
            return number1 / number2;
        default:
            return NaN;
    }
}

let number1 = getNumber();
let number2 = getNumber();

let operation = getSurgery();

let result = perfomOperation(number1, number2, operation);
console.log(`result: ${result}`);